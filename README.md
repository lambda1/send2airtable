# Send 2 Airtable
## AWS Lambda Edition

This is a fork of my original send2airtable modified to run on AWS Lambda . The idea is to be able to send links with categories straight to Airtable via Siri Shortcuts on iO. For example, you're browsing Twitter, find a tweet about a new interesting band you'll want to listen to at a later date, just hit share on ios and boom, it triggers our Lambda function and add a new link (with a title) in the right category of your airtable base for later consumption, a bit like you're own homebrew Pocket or Instapaper .

---


### Configuration

We need a *base* to point to, this will be our *config base*, we do this in order to be able to point to several bases for different categories but only call the config one from Lambda . ( Yes the table needs to be called **Table 1**, haven't made it more generic yet ...) .

![config](README_FILES/sc1.png)


You will need to create a file called **app-env.sh** at the root of the project to store ENV VARIABLES, we'll require :

- **AIRTABLE_API_KEY** , 
- **CONFIG_BASE**

### Testing

Testing is done with docker using [this](https://github.com/lambci/docker-lambda) image, which mimicks AWS Lambda's environment very well .

Run **run.sh** to test the function .


### Deployment

We deploy to Lambda with the [AWS CLI](https://aws.amazon.com/cli/) and a bit of shell scripting to package everything up nicely in a zip file .


### Calling the API with a Siri Shortcut