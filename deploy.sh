#!/bin/bash

cd package
zip -r9 ${OLDPWD}/function.zip .

cd $OLDPWD
zip -g function.zip function.py

aws lambda update-function-code --function-name send2airtable --zip-file fileb://function.zip