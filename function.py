import airtable
import urllib.request
import argparse
import os
import json
import logging
from lxml import etree

parser = argparse.ArgumentParser()
parser.add_argument("-t")
parser.add_argument("-l")
args = parser.parse_args()

def get_table_id(category_table):
    categories = airtable.Airtable(category_table, "Table 1")

    results = {}
    for i in categories.get_all():
        results[i['fields']['Name']] = i['fields']['tableid']

    return results


def parse_url(url, category):

    if "twitter.com/" in url:
        name = url.split('twitter.com/')[1].split('/')[0]

    elif "youtu" in url:
        youtube = etree.HTML(urllib.request.urlopen(url).read())
        video_title = youtube.xpath("//span[@id='eow-title']/@title")
        name = (''.join(video_title))
    else:
        name = ""

    record = {'Name': name, 'URL': url, 'Tag': category}

    return record


def send(event, context):
    # category_parameter = event['queryStringParameters']['category']
    # link_parameter = event['queryStringParameters']['link']

    category_parameter = event['headers']['category']
    link_parameter = event['headers']['link']


    table_id = get_table_id(os.environ['CONFIG_BASE'])
    category = airtable.Airtable(table_id[category_parameter], "Table 1")
    record = parse_url(link_parameter, category_parameter)
    message = category.insert(record)

    return {
        'message': json.dumps(message)
    }
